// const mongoose = require('mongoose')
// const dotenv = require('dotenv')
// dotenv.config({ path:'./config.env' })
// const app = require('./app')

// const DB = process.env.DATABASE.replace(
//     'PASSWORD',
//     process.env.DATABASE_PASSWORD,
// )

// const local_DB = process.env.DATABASE_LOCAL
// //console.log(process.env.DATA_PASSWORD)
// mongoose.connect(local_DB).then((con) => {
//     // console.log(con.connections)
//     console.log('DB connection successul')

// }).catch(error => console.log(error));

// /*starting the server on the port 4001 */
// const port = 4001
// app.listen(port, () => {
//     console.log(`App running on port ${port} ..`)
// })

const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({ path:'./config.env' })
const app = require('./app')

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD,
)
//console.log(process.env.DATA_PASSWORD)
mongoose.connect(DB).then((con) => {
    console.log(con.connections)
    console.log('DB connection successul')

}).catch(error => console.log(error));

/*starting the server on the port 4001 */
const port = 4001
app.listen(port, () => {
    console.log(`App running on port ${port} ..`)
})
